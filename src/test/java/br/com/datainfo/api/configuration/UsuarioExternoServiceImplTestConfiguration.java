package br.com.datainfo.api.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import br.com.datainfo.api.service.UsuarioExternoService;

@TestConfiguration
public class UsuarioExternoServiceImplTestConfiguration {
	
	@Bean
	public UsuarioExternoService userService() {
		return new UsuarioExternoService();
	}

}
