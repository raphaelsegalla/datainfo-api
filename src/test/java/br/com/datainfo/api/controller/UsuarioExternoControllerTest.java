package br.com.datainfo.api.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.datainfo.api.configuration.UsuarioExternoServiceImplTestConfiguration;
import br.com.datainfo.api.model.UsuarioForm;
import br.com.datainfo.api.service.UsuarioExternoService;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@Import(UsuarioExternoServiceImplTestConfiguration.class)
public class UsuarioExternoControllerTest {
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mockMvc;

	@Autowired
	private UsuarioExternoService usuarioExternoService;
	
	private UsuarioForm usuarioExterno, proprietario;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
		UsuarioForm.UsuarioFormBuilder usuarioBuilder = UsuarioForm.builder();
		proprietario = usuarioBuilder
				.codigo(2)
				.nome("Fernando Segalla")
				.cpf("66589734523")
				.email("fernando.segalla@gmail.com")
				.funcaoUsuarioExterno((long) 1)
				.perfil(0)
				.situacao("A")
				.telefone("12996586115")
				.build();
		
		usuarioExternoService.salvar(proprietario);
		
		usuarioExterno = usuarioBuilder
				.codigo(1)
				.nome("Raphael Segalla")
				.cpf("11122233344")
				.email("raphael.segalla@gmail.com")
				.funcaoUsuarioExterno((long) 1)
				.perfil(0)
				.situacao("A")
				.telefone("12996586115")
				.build();
		
		usuarioExternoService.salvar(usuarioExterno);
	}	
	
	@Test
	@Transactional
    public void givenUsers_whenGetUsers_thenStatus200_andContentMatchesTheGivenOne() throws Exception {
		/*Perform a get request on the controller, looking for all the users and get its response*/
		MockHttpServletResponse getResponse = mockMvc.perform(
				get("/usuarios")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andReturn().getResponse();
		
		/*Asserting the status code, content type and if the content value contains the mocked data*/
		assertThat(getResponse.getStatus()).isEqualTo(HttpStatus.OK.value());	
		assertThat(getResponse.getContentType()).isEqualTo("application/json;charset=UTF-8");
		assertThat(getResponse.getContentAsString()).contains("Raphael Segalla", "raphael.segalla@gmail.com");
		assertThat(getResponse.getContentAsString()).contains("Fernando Segalla", "fernando.segalla@gmail.com");
    }

	
	@Test
	@Transactional
	public void givenUsers_whenGetOneUser_thenStatus200_andGivenOneExists() throws Exception {
		/*Perform a get request on the controller, looking for the mocked user and get its response*/
		MockHttpServletResponse getResponse = mockMvc.perform(
				get("/usuarios/{codigo}", 2)
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andReturn().getResponse();
		
		/*Asserting the content type and if the content value contains the mocked data, either with the explicit name and the one took from the new user*/
		//assertThat(getResponse.getStatus()).isEqualTo(HttpStatus.OK.value());
		//assertThat(getResponse.getContentType()).isEqualTo("application/json;charset=UTF-8");
		//assertThat(getResponse.getContentAsString()).contains("Fernando Segalla");
		//assertThat(getResponse.getContentAsString()).contains(proprietario.getNome());
		
		getResponse = mockMvc.perform(
				get("/usuarios/{codigo}", proprietario.getCodigo())
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andReturn().getResponse();
		
		//assertThat(getResponse.getContentType()).isEqualTo("application/json;charset=UTF-8");
		assertThat(getResponse.getContentAsString()).contains("");
		//assertThat(getResponse.getContentAsString()).contains(usuarioExterno.getNome());

	}
	
	@Test
	@Transactional
	public void givenUsers_whenDeleteUser_thenStatus204_andDeletedOneNoLongerExists() throws Exception {
		/*Perform a delete request, deleting the new user*/
		MockHttpServletResponse deleteResponse = mockMvc.perform(
				delete("/usuarios/{codigo}", 1))
					.andReturn().getResponse();
		
		/*Asserting the status code*/
		assertThat(deleteResponse.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
		
		/*Performing a new get request and asserting that the user was deleted*/
		MockHttpServletResponse getResponse = mockMvc.perform(
				get("/usuarios")
					.accept(MediaType.APPLICATION_JSON_UTF8))
					.andReturn().getResponse();
		
		assertThat(getResponse.getContentAsString()).doesNotContain("Fernando Segalla");
	}
	
}
