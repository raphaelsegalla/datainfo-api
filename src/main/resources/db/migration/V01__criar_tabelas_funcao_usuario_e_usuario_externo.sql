CREATE TABLE funcao_usuario_externo (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	funcao VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE usuario_externo (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	cpf VARCHAR(11) NOT NULL UNIQUE,
	nome VARCHAR(60) NOT NULL,
	email VARCHAR(255) NOT NULL,
	situacao VARCHAR(1) NOT NULL,
	perfil BIGINT(2) NOT NULL,
	codigo_funcao BIGINT(20) NOT NULL,
	telefone VARCHAR(11),
	FOREIGN KEY (codigo_funcao) REFERENCES funcao_usuario_externo(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO funcao_usuario_externo (funcao) values ('Gestor');
INSERT INTO funcao_usuario_externo (funcao) values ('Administrador');
INSERT INTO funcao_usuario_externo (funcao) values ('Frente de Caixa');
