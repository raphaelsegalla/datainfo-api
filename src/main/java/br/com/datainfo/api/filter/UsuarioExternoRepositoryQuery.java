package br.com.datainfo.api.filter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.datainfo.api.model.UsuarioExterno;

public interface UsuarioExternoRepositoryQuery {
	
	public Page<UsuarioExterno> filtrar(UsuarioExternoFilter usuarioFilter, Pageable pageable);

}
