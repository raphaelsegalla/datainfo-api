package br.com.datainfo.api.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.datainfo.api.model.UsuarioExterno;

@Service
public class UsuarioExternoRepositoryImpl implements UsuarioExternoRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<UsuarioExterno> filtrar(UsuarioExternoFilter usuarioFilter, Pageable pageable) {
		
		System.out.println(usuarioFilter.getPerfil());
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<UsuarioExterno> criteria = builder.createQuery(UsuarioExterno.class);
		Root<UsuarioExterno> root = criteria.from(UsuarioExterno.class);
		
		Predicate[] predicates = criarRestricoes(usuarioFilter, builder, root);
		criteria.where(predicates).distinct(true);
		
		TypedQuery<UsuarioExterno> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		return new PageImpl<>(query.getResultList(), pageable, total(usuarioFilter));
	}
	
	private Predicate[] criarRestricoes(UsuarioExternoFilter usuarioFilter, CriteriaBuilder builder, Root<UsuarioExterno> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(usuarioFilter.getNome()) ) {
			predicates.add(builder.like(builder.lower(root.get("nome")), "%" + usuarioFilter.getNome().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(usuarioFilter.getSituacao()) ) {
			predicates.add(builder.like(builder.lower(root.get("situacao")), "%" + usuarioFilter.getSituacao().toLowerCase() + "%"));
		}
		
		if ( usuarioFilter.getPerfil() >= 0) {
			predicates.add(builder.equal(root.<String>get("perfil"), String.valueOf(usuarioFilter.getPerfil())));			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<UsuarioExterno> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(UsuarioExternoFilter usuarioFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<UsuarioExterno> root = criteria.from(UsuarioExterno.class);
		
		Predicate[] predicates = criarRestricoes(usuarioFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
