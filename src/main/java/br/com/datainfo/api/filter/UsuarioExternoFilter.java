package br.com.datainfo.api.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioExternoFilter {
	
	private String nome;
	
	private String situacao;
	
	private int perfil;

}
