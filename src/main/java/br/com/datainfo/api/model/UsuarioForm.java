package br.com.datainfo.api.model;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class UsuarioForm {
	
	private long codigo;
		
	@NotNull
	private String cpf;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String email;
	
	@NotNull
	private String situacao;
	
	@NotNull
	@Column(name = "perfil")
	private int perfil;
	
	@NotNull
	private Long funcaoUsuarioExterno;
	
	private String telefone;

}
