package br.com.datainfo.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder(toBuilder = true)
@Table(name = "usuario_externo")
public class UsuarioExterno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	@Column(name = "cpf")
	private String cpf;
	
	@NotNull
	@Column(name = "nome")
	private String nome;
	
	@NotNull
	@Column(name = "email")
	private String email;
	
	@NotNull
	@Column(name = "situacao")
	private String situacao;
	
	@NotNull
	@Column(name = "perfil")
	private int perfil;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_funcao")
	private FuncaoUsuarioExterno funcaoUsuarioExterno;
	
	@Column(name = "telefone")
	private String telefone;
}
