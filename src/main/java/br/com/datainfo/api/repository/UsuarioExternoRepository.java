package br.com.datainfo.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.datainfo.api.filter.UsuarioExternoRepositoryQuery;
import br.com.datainfo.api.model.UsuarioExterno;

public interface UsuarioExternoRepository extends JpaRepository<UsuarioExterno, Long>, UsuarioExternoRepositoryQuery {

}
