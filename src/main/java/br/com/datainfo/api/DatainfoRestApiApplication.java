package br.com.datainfo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatainfoRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatainfoRestApiApplication.class, args);
	}

}
