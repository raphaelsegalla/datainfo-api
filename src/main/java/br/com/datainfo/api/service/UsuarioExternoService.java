package br.com.datainfo.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.datainfo.api.model.FuncaoUsuarioExterno;
import br.com.datainfo.api.model.UsuarioExterno;
import br.com.datainfo.api.model.UsuarioForm;
import br.com.datainfo.api.repository.UsuarioExternoRepository;
import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class UsuarioExternoService {
	
	@Autowired
	private UsuarioExternoRepository usuarioExternoRepository;
	
	public UsuarioExterno atualizar(Long codigo, UsuarioForm usuarioForm) {
		usuarioForm.setTelefone(usuarioForm.getTelefone().replaceAll("[^0-9]", ""));
		usuarioForm.setCpf(formatarCpf(usuarioForm.getCpf()));
		Optional<UsuarioExterno> usuarioSalvo = usuarioExternoRepository.findById(codigo);
		BeanUtils.copyProperties(usuarioForm, usuarioSalvo.get(), "codigo");
		return usuarioExternoRepository.save(usuarioSalvo.get());	
	}
	
	public void ativaOuDesativaUsuario(Long codigo) throws ObjectNotFoundException {
		UsuarioExterno usuarioSalvo = usuarioExternoRepository.findById(codigo).orElseThrow(() -> new ObjectNotFoundException("Este usuário não existe"));
		if(usuarioSalvo.getSituacao().equalsIgnoreCase("A")) {
			usuarioSalvo.setSituacao("I");
		}else{
			usuarioSalvo.setSituacao("A");
		}
		usuarioExternoRepository.save(usuarioSalvo);
	}
	
	public UsuarioExterno salvar(UsuarioForm usuarioForm) {
		UsuarioExterno usuario = new UsuarioExterno();
		FuncaoUsuarioExterno funcao = new FuncaoUsuarioExterno();
		
		funcao.setCodigo(usuarioForm.getFuncaoUsuarioExterno());
		
		usuario.setCpf(formatarCpf(usuarioForm.getCpf()));
		usuario.setNome(usuarioForm.getNome());
		usuario.setEmail(usuarioForm.getEmail());
		usuario.setPerfil(usuarioForm.getPerfil());
		usuario.setSituacao(usuarioForm.getSituacao());
		usuario.setFuncaoUsuarioExterno(funcao);
		usuario.setTelefone(usuarioForm.getTelefone().replaceAll("[^0-9]", ""));
		
		return usuarioExternoRepository.save(usuario);
	}
	
	private String formatarCpf(String cpf) {
		
		cpf = cpf.replace( " " , "");
		cpf = cpf.replace( "." , "");
		cpf = cpf.replace( "-" , "");
		
		return cpf;
	}
}
