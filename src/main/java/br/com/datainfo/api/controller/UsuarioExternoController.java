package br.com.datainfo.api.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.datainfo.api.filter.UsuarioExternoFilter;
import br.com.datainfo.api.model.UsuarioExterno;
import br.com.datainfo.api.model.UsuarioForm;
import br.com.datainfo.api.repository.UsuarioExternoRepository;
import br.com.datainfo.api.service.UsuarioExternoService;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping("/usuarios")
public class UsuarioExternoController {
	
	@Autowired
	private UsuarioExternoRepository usuarioExternoRepository;
	
	@Autowired
	private UsuarioExternoService usuarioExternoService;
	
	@GetMapping public Page<UsuarioExterno> listar(UsuarioExternoFilter usuarioFilter, Pageable pageable) { 
		return usuarioExternoRepository.filtrar(usuarioFilter, pageable); 
	}
	 
	@GetMapping("/{codigo}") 
	public ResponseEntity<UsuarioExterno> buscarPeloCodigo(@PathVariable Long codigo) { 
		Optional<UsuarioExterno> usuario = usuarioExternoRepository.findById(codigo); 
		return usuario.isPresent() ? ResponseEntity.ok(usuario.get()) : ResponseEntity.notFound().build(); 
	}
	
	@PostMapping
	public ResponseEntity<UsuarioExterno> registrar(@RequestBody UsuarioForm usuarioForm) {
		System.out.println(usuarioForm);
		UsuarioExterno usuarioSalvo = usuarioExternoService.salvar(usuarioForm);
		return usuarioSalvo != null ? ResponseEntity.ok(usuarioSalvo) : ResponseEntity.notFound().build(); 
	}
	
	@PostMapping("/{codigo}")
	public void mudarSituacao(@PathVariable Long codigo) throws ObjectNotFoundException {
		usuarioExternoService.ativaOuDesativaUsuario(codigo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		usuarioExternoRepository.deleteById(codigo);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<UsuarioExterno> atualizar(@PathVariable Long codigo, @Valid @RequestBody UsuarioForm usuarioForm) {
		UsuarioExterno usuarioSalvo = usuarioExternoService.atualizar(codigo, usuarioForm);
		return ResponseEntity.ok(usuarioSalvo);
	}

}
